#version=DEVEL
# use a fedora mirror
url --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch
# System authorization information
auth --enableshadow --passalgo=sha512
# Use graphical install
graphical
# Don't run the Setup Agent on first boot
# the part coming up is actually gnome-initial-setup
firstboot --disabled
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# reboot
reboot

# Network information
network  --bootproto=dhcp --device=ens192 --ipv6=auto --activate
network  --hostname=envy24
# Root password
rootpw --iscrypted $6$FeJz1TNbSHwYrgWD$740RAVvgxfzCbP4ySdUOoMHafjkm4DIN0JxZWRq.xyZpUTfZYTkuWJ5JKDOp3mZqDB.4L4YwwEx7TV1RVbvEb/
# System services
services --enabled="chronyd,sshd"
# System timezone
timezone America/New_York --isUtc
# X Window System configuration information
xconfig  --startxonboot
# System bootloader configuration
bootloader --location=mbr --boot-drive=sda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=sda

%packages
@^workstation-product-environment
@ansible-node
ansible
chrony
# don't install gnome-initial-setup
-gnome-initial-setup

%end

%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy user --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
%end

%post
# doubly attempt to stop the screen on startup
# REFERENCE: https://bugzilla.redhat.com/show_bug.cgi?id=1067653#c14
echo "[daemon]
InitialSetupEnable=False" | /usr/bin/tee -a /etc/gdm/custom.conf;
%end
