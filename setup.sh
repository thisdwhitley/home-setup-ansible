#!/bin/bash
### setup.sh ###>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>#
# DESCRIPTION:  
# DEPENDANCY:   
# RESULT:       
# USE:		
# REFERENCE:    
# CONTACT:      
#------------------------------------------------------------------------------#

## these global variables can be used throughout ##++++++++++++++++++++++++++++#
space="echo #========="; 
scriptName="setup.sh";
repoURL="https://gitlab.com/ilearnedthisthehardway/home-setup-ansible.git";

$space;  
echo "#==| Starting $scriptName";

# this file will be called remotely (I will have to figure out permissions...)


## check the OS type and set some variables based on that ##+++++++++++++++++++#
echo -e "#=|  Checking OS:  ";
OS=$(awk '/^ID[^_]/ {split($0,items,"="); print tolower(items[2])}' /etc/*release);
case $OS in
  fedora)   pkgMgr="dnf"; role="laptop"; 
            OSspecPkg="python2-dnf libselinux-python";; 
  ubuntu)   pkgMgr="apt-get"; role="htpc" 
            OSspecPkg="sshpass";; 
  raspbian) pkgMgr="apt-get"; role="pi";; 
  *)        echo "...I don't know what OS this system is running"; exit;; 
esac

echo "This is $OS [using $pkgMgr and $role role]";


## install the initial required packages ##++++++++++++++++++++++++++++++++++++#
# assume this will be run as root so no "sudo" necessary
echo "#=|  Installing git and ansible:  $pkgMgr install -y git ansible";
$pkgMgr install -y git ansible $OSspecPkg;

echo "#-|  Installing additional packages, but this will require additional research about what is need for each OS:"
#$pkgMgr install -y python2-dnf;


## pull down the git repository ##+++++++++++++++++++++++++++++++++++++++++++++#
echo "#=|  Cloning git repository:";
git clone $repoURL;


## update ssh config for ansible ##++++++++++++++++++++++++++++++++++++++++++++#
#  per https://ansible-tips-and-tricks.readthedocs.io/en/latest/ansible/install/
echo "#=|  Configuring SSH for root so ansible doesn't complain."
[[ -d /root/.ssh ]] || mkdir -vp /root/.ssh/
cat <<SSH>>/root/.ssh/config
Host localhost*
  Port 22
  User root
  StrictHostKeyChecking=no
  UserKnownHostsFile=/dev/null
SSH


## running ansible playbook ##+++++++++++++++++++++++++++++++++++++++++++++++++#
echo "#=|  Running ansible playbook for $role:";
cd home-setup-ansible;
ansible-playbook -k -u root --ask-vault-pass -i hosts -l myself ${role}.yml

#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<### setup.sh ###