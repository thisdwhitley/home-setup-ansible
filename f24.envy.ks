#version=DEVEL
# use a fedora mirror
url --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch
# System authorization information
auth --enableshadow --passalgo=sha512
# Use graphical install
graphical
# Don't run the Setup Agent on first boot
# the part coming up is actually gnome-initial-setup
firstboot --disabled
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# reboot
reboot

# Network information
network  --bootproto=dhcp --device=wlp3s0 --ipv6=auto --activate
network  --hostname=envy24 --essid=CliffYablonski --wepkey=0ctopusTest!cles
# Root password
rootpw --iscrypted $6$FeJz1TNbSHwYrgWD$740RAVvgxfzCbP4ySdUOoMHafjkm4DIN0JxZWRq.xyZpUTfZYTkuWJ5JKDOp3mZqDB.4L4YwwEx7TV1RVbvEb/
# System services
services --enabled="chronyd,sshd"
# System timezone
timezone America/New_York --isUtc
# X Window System configuration information
xconfig  --startxonboot
# System bootloader configuration
bootloader --location=mbr --boot-drive=sda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=sda
# Disk partitioning information
part pv.64 --fstype="lvmpv" --noformat --onpart=sda7
part /boot --fstype="ext4" --onpart=sda6 --label=BOOT
part pv.25 --fstype="lvmpv" --noformat --onpart=sda3
volgroup vg_envy --noformat --useexisting
volgroup vg_extra --noformat --useexisting
logvol /extra  --fstype="ext4" --noformat --useexisting --name=lv_extra --vgname=vg_extra
logvol swap  --fstype="swap" --useexisting --name=swap --vgname=vg_envy
logvol /  --fstype="ext4" --useexisting --label="ROOT" --name=lv_root --vgname=vg_envy


%packages
@^workstation-product-environment
@ansible-node
ansible
chrony
# don't install gnome-initial-setup
-gnome-initial-setup

%end

%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy user --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=0 --minquality=1 --notstrict --nochanges --emptyok
%end

%post
# doubly attempt to stop the screen on startup
# REFERENCE: https://bugzilla.redhat.com/show_bug.cgi?id=1067653#c14
echo "[daemon]
InitialSetupEnable=False" | /usr/bin/tee -a /etc/gdm/custom.conf;
%end
