An attempt at automating my builds...
=====================================

The current approach is going to be:
1. install basic system (potentially using KS)
2. once, up, log in and run setup.sh command from this repo:
```
wget -qO- https://gitlab.com/ilearnedthisthehardway/home-setup-ansible/raw/master/setup.sh | /bin/bash
```

---

I have found a flaw in my original approach...the version of ansible will change depending on where I run it from.  So using ansible to "push" a configuration to a newly built system will unfortunately be dependent of the host ansible is being run from.  This is not ideal.

So I must change my approach and see these alternatives:
- run ansible FROM the newly built host to configure itself (https://blog.josephkahn.io/articles/ansible/)
- create a container with a version of ansible I deem appropriate and install/run that container from a newly built host to configure itself (https://medium.com/@tech_phil/running-ansible-inside-docker-550d3bb2bdff#.7tmx68i53)

This repository is my attempt at automating the configuration of my environment which currently consists of a Fedora laptop, a Ubuntu HTPC, and a Raspbian Raspberry Pi.

At this point, I am thinking that I will rebuild my Fedora laptop using a generic Kickstart file.  Then, from some other system (htpc?), pull down this repository:
```bash
git clone https://ilearnedthisthehardway@gitlab.com/ilearnedthisthehardway/home-setup-ansible.git /tmp/home-setup-ansible
```
Then make sure the hosts file is correct and run the appropiate playbook, as I'm testing, I am doing:
```bash
cd /tmp/home-setup-ansible/
ansible-playbook -k -u root --ask-vault-pass -i hosts -l envy24 laptop.yml
```
At this poing I am running the playbook as `root` because that is the only user when the system is initially installed.

## Common

### Fedora laptop

- [ ] change terminal settings (smaller font, different color)
- [ ] change file viewer preferences to be details
- [ ] install dropbox
- [ ] install sublime with git pacakge
- [ ] configure wireless (or is this part of KS?)
- [ ] disable lockscreen
- [ ] add items to "favorites"
- [ ] make chrome default browser and sublime default editor
- [ ] change the default colors of sublime to match terminal
- [x] configure 2 screens
- [x] mount /extra (commented out for testing)
- [x] configure printer (Samsung M288x)
- [x] configure scanner [gscan2pdf detects it after driver install)
- [x] install chrome
- [x] create links in /home/daniel
- [x] install lastpass [already part of chrome when I sign in as myself)
- [x] remove rhgb quiet from grub

### Ubuntu HTPC

Raspberry Pi
------------

To use the pi.yml file, I need to run it as the default pi user, but I need to install items as root and I need to ask for the vault password.  So it looks like:

```
ansible-playbook --ask-vault --ask-become -k -u pi -i hosts -l raspberrypi pi.yml
```

* I had to disable the OS version check on the nfs-client installation role
* I had to move the start of rpcbind to the debian.yml task instead of handler


REFERENCES:
* check out:  https://github.com/ksylvan/AnsibleLaptop


testing
