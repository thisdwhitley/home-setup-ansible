sublime
=======

This role will install Sublime Text, ensure it shows up in the Applications window of Gnome, and install the Git package for use withing Sublime Text.

Requirements
------------

This role was created for a Fedora system (Fedora 24 as of this writing).  Both Ansible and Git must already be installed.

Role Variables
--------------

I have set a variable named sublime_version in defaults/main.yml

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

References
----------

* [Sublime Text download page](https://www.sublimetext.com/3)
* [For some hints...but really learned by just extracting](https://digitizor.com/install-sublime-text-editor-3-fedora-redhat/)
* [Connecting Sublime with Git](https://scotch.io/tutorials/using-git-inside-of-sublime-text-to-improve-workflow)
* [Installing the Git plugin](https://github.com/kemayo/sublime-text-git/wiki)

Author Information
------------------

ilearnedthisthehardway.com

Current Status
--------------

Unable to get the icon in the favorites dash.  I have been able to achieve this on the system (outside of ansible) by doing the following:
```
[daniel@envy24 ~]$ gsettings get org.gnome.shell favorite-apps
['firefox.desktop', 'sublime-text-3.desktop', 'shotwell.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Software.desktop', 'sublime-text-3.desktop']
[daniel@envy24 ~]$ NEWFAV=$(gsettings get org.gnome.shell favorite-apps | sed "s/']/', 'google-chrome.desktop']/")
[daniel@envy24 ~]$ gsettings get org.gnome.shell favorite-apps
['firefox.desktop', 'sublime-text-3.desktop', 'shotwell.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Software.desktop', 'sublime-text-3.desktop']
[daniel@envy24 ~]$ DISPLAY=:0 gsettings set org.gnome.shell favorite-apps "$(echo $NEWFAV)"
[daniel@envy24 ~]$ gsettings get org.gnome.shell favorite-apps
['firefox.desktop', 'sublime-text-3.desktop', 'shotwell.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Software.desktop', 'sublime-text-3.desktop', 'google-chrome.desktop']
[daniel@envy24 ~]$ DISPLAY=:0 dconf update
[daniel@envy24 ~]$ gsettings get org.gnome.shell favorite-apps
['firefox.desktop', 'sublime-text-3.desktop', 'shotwell.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Software.desktop', 'sublime-text-3.desktop', 'google-chrome.desktop']
```
Doing this manually even persisted after a reboot...

At one point I was getting the errors below, but I am not anymore...:
```
TASK [sublime : Create some links] *********************************************
changed: [localhost] => (item={u'dest': u'/opt/sublime_text', u'src': u'/opt/sublime_text_3'})
failed: [localhost] (item={u'dest': u'/usr/share/applications/sublime-text-3.desktop', u'src': u'/opt/sublime_text/sublime_text.desktop'}) => {"failed": true, "gid": 0, "group": "root", "item": {"dest": "/usr/share/applications/sublime-text-3.desktop", "src": "/opt/sublime_text/sublime_text.desktop"}, "mode": "0644", "msg": "refusing to convert between file and link for /usr/share/applications/sublime-text-3.desktop", "owner": "root", "path": "/usr/share/applications/sublime-text-3.desktop", "secontext": "unconfined_u:object_r:usr_t:s0", "size": 243, "state": "file", "uid": 0}
```

What I learned
--------------

Using gsettings remotely (such as through SSH via Ansible) fails quietly.  I had to run the playbook with a `-v` in order to capture the information below.  From that I researched it and found that I need to set a DISPLAY prior to using `gsetting set`.  [REFERENCE](https://ubuntuforums.org/showthread.php?t=2199429)

I also learned that I needed to use REGISTER.stderr with debug to see the second message below.

When trying to update the "favorites" I am getting the following:
```
TASK [sublime : Add Sublime to favorite-apps (per user) with gsettings] ********
changed: [localhost] => {"changed": true, "cmd": "gsettings set org.gnome.shell favorite-apps \"['firefox.desktop', 'evolution.desktop', 'rhythmbox.desktop', 'shotwell.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Software.desktop', 'sublime-text-3.desktop']\"", "delta": "0:00:00.019676", "end": "2016-08-26 13:49:25.364670", "rc": 0, "start": "2016-08-26 13:49:25.344994", "stderr": "\n(process:14098): dconf-WARNING **: failed to commit changes to dconf: Error spawning command line 'dbus-launch --autolaunch=97c83a8a9a4e4c84ae35850f85ace33b --binary-syntax --close-stderr': Child process exited with code 1", "stdout": "", "stdout_lines": [], "warnings": []}

TASK [sublime : debug] *********************************************************
ok: [localhost] => {
    "msg": "gsettings set output \n(process:14098): dconf-WARNING **: failed to commit changes to dconf: Error spawning command line 'dbus-launch --autolaunch=97c83a8a9a4e4c84ae35850f85ace33b --binary-syntax --close-stderr': Child process exited with code 1"
```

