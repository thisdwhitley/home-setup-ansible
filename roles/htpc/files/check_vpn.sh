#!/bin/bash
# This script will check to see if the system is getting network connectivity 
# through the VPN.  If the network is either inaccessible or if the IP I am 
# talking out of is in Raleigh, I will attempt to restart openvpn a set number
# of times and then reboot.  Sort of extreme, but it should come back up with 
# the correct network information.  
##
# This script will likely be called from a cronjob

max_tries="5";
tries="0";
bad_location="Raleigh";

while [[ $tries -lt $max_tries ]]; do
  location=$(curl -ks https://www.dnsleaktest.com | awk '/flag/ {gsub(",","",$2); print $2}');
  if [[ -z $location ]]; then
    echo "#-! $(date +%Y-%m-%d:%H:%M:%S) | [$tries] Unable to determine location...restarting VPN" >> /root/vpn.log;
    #systemctl restart openvpn; # for some reason this isn't working
    /usr/sbin/openvpn /etc/openvpn/client.conf &
    (( tries++ ));  sleep 5;
  elif [[ "$location" =~ "$bad_location" ]]; then
    echo "#-! $(date +%Y-%m-%d:%H:%M:%S) | [$tries] Bad location ($location)...restarting VPN" >> /root/vpn.log;
    /usr/sbin/openvpn /etc/openvpn/client.conf &
    (( tries++ ));  sleep 5;
  else 
    break;
  fi;
done;

# at this point, we've attempted to restart the VPN a few times...reboot!
if [[ $tries -eq $max_tries ]]; then
  echo "#-! $(date +%Y-%m-%d:%H:%M:%S) | The VPN has been restarted $tries times yet it is not working.  Rebooting!" >> /root/reboot.log;
  # configure rc.local to send an email when the system comes back online:
  echo "echo -e \"Subject: htpc rebooted - $(date)\r\n\r\n$(tail -n1 /root/reboot.log)\" | /usr/sbin/sendmail -F htpc camellia.email@gmail.com" >> /etc/rc.local;
  echo "sed -i '/email/d' /etc/rc.local;" >> /etc/rc.local;
  /sbin/reboot;
fi;

exit;
